import { Component, OnInit, Input } from '@angular/core';
import { AdModel } from 'src/app/shared/models/ad.model';

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {
  @Input('ad') ad: AdModel;

  constructor() { }

  ngOnInit() {
  }

}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider/slider.component';
import { InformationsComponent } from './informations/informations.component';
import { AdsComponent } from './ads/ads.component';

@NgModule({
  declarations: [SliderComponent, InformationsComponent, AdsComponent],
  imports: [
    CommonModule
  ],
  exports: [SliderComponent,InformationsComponent, AdsComponent]
})
export class ComponentsModule { }

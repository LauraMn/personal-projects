import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [HomeComponent, LoginComponent],
  imports: [
    ComponentsModule,
    CommonModule
  ]
})
export class FeaturesModule { }

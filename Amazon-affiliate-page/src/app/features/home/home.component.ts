import { Component, OnInit } from '@angular/core';
import { AdModel } from 'src/app/shared/models/ad.model';
import { AdArray } from 'src/assets/data/ad-data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public adList: AdModel[];
  constructor() { }

  ngOnInit() {
    this.adList = AdArray;
  }

}

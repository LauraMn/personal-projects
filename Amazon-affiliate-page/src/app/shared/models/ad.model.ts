export class AdModel {
  title: string;
  message: string;
  link: string;
  image?: string;

  constructor (value: Object = {}) {
    Object.assign(this, value)
  }
}
import { AdModel } from "src/app/shared/models/ad.model";

export const AdArray: AdModel[] = [
  {
    title: 'Boost your earnings with Native Shopping Ads',
    message:'Display highly relevant and dynamic product recommendations in a stylishly designed, responsive and mobile-optimized ad unit.',
    link: ''
  },
  {
    title:'Amazon Bounty Program',
    message:'It’s easy. Refer and earn fixed advertising fees when visitors try and/or sign-up for valuable services and programs.',
    link: 'Get Links and Banners',
    image: ''
  }
]
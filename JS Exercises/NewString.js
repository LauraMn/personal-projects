//Write a JavaScript program to create a new string without the first and last character of a given string.
function str(n){
  return n.slice(1, n.length - 1);
}
console.log(str("JavaScript"));